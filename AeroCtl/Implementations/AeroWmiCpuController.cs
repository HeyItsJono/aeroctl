﻿using System.Threading.Tasks;

namespace AeroCtl;

/// <summary>
/// CPU control exposed through the Aero WMI interface.
/// </summary>
public class AeroWmiCpuController : ICpuController
{
	private readonly AeroWmi wmi;

	public AeroWmiCpuController(AeroWmi wmi)
	{
		this.wmi = wmi;
	}

	public async ValueTask<double> GetTemperatureAsync()
	{
		return await this.wmi.InvokeGetAsync<ushort>("getCpuTemp");
	}
}